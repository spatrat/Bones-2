'use strict';
const requireDir = require('require-dir');

// Require all tasks in gulpfile.js/tasks, including subfolders
requireDir('./gulpfile/tasks', { recurse: true });