// Styleguide - Module Selector
import $ from 'jquery';

export default class StyleguideModule {
    constructor(el) {
        this.el = el;
        const $el = $(el);
        const modules = document.querySelectorAll('[data-tags]');
        console.log(modules);
        const ml = modules.length;
        const tags = [];

        if ( ml > 0 ) {
            for ( let i = 0; i < ml; i++ ) {

                const mod = modules[i];

                mod.getAttribute('data-tags').split(' ').map( function(tag) {
                    return tags.push(tag);
                });

            }
        }

        const tagsFiltered = tags.filter(function(item, pos, self) {
            return self.indexOf(item) == pos;
        });

        const tagsSorted = tagsFiltered.sort();

        tagsSorted.map( function(tag) {
            return createOption($el, tag);
        });

        $el.on('change', function() {
            const val = $el.val();

            $('[data-tags].sg-module-tag--'+ val).css('display', 'block');
            $('[data-tags]').not('.sg-module-tag--'+ val).css('display', 'none');

            if (val === "*") {
                $('[data-tags]').css('display', 'block');
            }
        });

        function createOption(el,opt) {
            let $option = $(`<option value="${opt}">${opt}</option>`)
            $el.append($option);
        }

    }
}