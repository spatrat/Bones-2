const navbarVisibleClasses = 'collapsed';
const stateUpdated = new Event('stateUpdated');

export default class NavbarToggle {
    constructor(el) {
        this.el = el;
        const self = this;

        const navbarLinked = document.getElementById(el.getAttribute('data-target'))

        const initialState =  {
            navbarVisible: el.dataset.targetVisible === 'true',
            exampleProperty: 'Example property value'
        };

        this.state = initialState;
        //console.log(this.state);

        this.stateUpdate = function() {
            el.dispatchEvent(stateUpdated);
        };

        this.setState = function (newState) {
            self.state =  Object.assign(self.state, newState );
            self.stateUpdate();
        };


        el.addEventListener('click', function() {
            self.toggleNavigation();
        });

        el.addEventListener('stateUpdated', function() {
            //console.log('State updated : ', self.state);
            el.classList.remove(navbarVisibleClasses);
            el.setAttribute('data-target-visible', self.state.navbarVisible );
            el.setAttribute('aria-expanded', self.state.navbarVisible );
            navbarLinked.setAttribute('aria-expanded', self.state.navbarVisible );
            if (self.state.navbarVisible === true) {
                navbarLinked.classList.add('in');
            } else {
                navbarLinked.classList.remove('in');
            }

        });

    }


    toggleNavigation() {
        this.setState({navbarVisible: !this.state.navbarVisible});
    }

    openNavigation() {
        this.setState({navbarVisible: true});
    }

    closeNavigation() {
        this.setState({navbarVisible: false});
    }

}

