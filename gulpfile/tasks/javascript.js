const gulp = require('gulp');
const del = require('del');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require('../../webpack.config.js');


const jsClean = function() {
    return del([
        'dist/javascript/**/*'
    ]);
};

const jsTask = function() {
    console.log('Starting javascript task!');
  return gulp.src('./src/javascript/app.js')
      .pipe(webpackStream(webpackConfig), webpack)
      .pipe(gulp.dest('./dist/javascript'));
};


gulp.task('js:clean', jsClean);
gulp.task('js', ['js:clean'], jsTask);
