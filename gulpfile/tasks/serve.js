const gulp = require('gulp');
const browserSync = require('browser-sync').create();

const config = {
    browsersync: {
        // your array of files and folders to watch for changes
        watch: [
            './src/javascript/**/*.js',
            './src/stylesheets/**/*.scss',
            './src/**/*.html'
        ]
    }
};

// use default task to launch Browsersync and watch JS files
gulp.task('serve', ['js', 'html', 'css'], function () {

    // Serve files from the root of this project
    browserSync.init({
        server: {
            baseDir: "./dist/"
        }
    });

    // add browserSync.reload to the tasks array to make
    // all browsers reload after tasks are complete.
    gulp.watch("./src/javascript/**/*.js", ['js']);
    gulp.watch("./src/html/**/*.html", ['html']);
    gulp.watch( './src/stylesheets/**/*.scss', [ 'css' ] );
    gulp.watch( config.browsersync.watch ).on( 'change', function() {
        browserSync.reload({stream:true})
    });
});