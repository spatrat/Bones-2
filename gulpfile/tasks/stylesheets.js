const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const del = require('del');

const cssClean = function() {
    return del([
        'dist/stylesheets/**/*'
    ]);
};

const cssTask = function() {
    return gulp.src('./src/stylesheets/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/stylesheets'));
};

gulp.task('clean:css', cssClean);
gulp.task('css', ['clean:css'],  cssTask);