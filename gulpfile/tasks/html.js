const gulp = require('gulp');
const nunjucksRender = require('gulp-nunjucks-render');
const del = require('del');
const concat = require('gulp-concat');
const fs = require('fs');
const glob = require('glob-fs')({ gitignore: true });



// Get data from json
const nunjucksData = require('../../src/data/data.json');


// Setting nunjucks default
const nunjucksDefaults = {
    path: 'src/html',
    ext: '.html',
    data: nunjucksData,
    inheritExtension: false,
    envOptions: {
        watch: false
    },
    manageEnv: null,
    loaders: ''
};


const htmlTask = function() {
    console.log('Starting html task!');
    return gulp.src('./src/html/*.html')
        .pipe(nunjucksRender(
            nunjucksDefaults
        ))
        .pipe(gulp.dest('./dist'));

};

const writeInModules = function() {

    const modules = glob.readdirSync('./src/html/modules/*/**.html');
    console.log("Les modules suivants on étés trouvés :", modules);

    let moduleIncludeString = "";
    const pathPartToRemove = "src/html/";
    const moduleTemplate = '\n' +
        '<div class="sg-module   sg-module-tag--cards  sg-module-tag--blocks  " data-tags="cards blocks">\n' +
        '    <h3 class="sg-module__title">##{moduleTitle}##</h3>\n' +
        '    <h4 class="sg-module__subtitle">test of subtitle</h4>\n' +
        '    <div class="sg-module__description">\n' +
        '       <p>Hello world</p>\n' +
        '    </div>\n' +
        '     <div class="sg-module__content">\n' +
        '           ##{moduleInclude}##\n'+
        '     </div>\n' +
        '</div>\n';

    for (let i = 0; i < modules.length; i++) {
        let moduleName = modules[i].split('/')[modules[i].split('/').length-1].replace('.html', '').replace('-', ' ');
        moduleIncludeString += moduleTemplate.replace('##{moduleInclude}##', "{% include '"
            +  modules[i].replace(pathPartToRemove, "")  + "'%}" ).replace('##{moduleTitle}##', moduleName);
    }


    const buffer = new Buffer(moduleIncludeString);
    const path = './src/html/shared/modules.html';

    // open the file in writing mode, adding a callback function where we do the actual writing
    return fs.open(path, 'w', function(err, fd) {
        if (err) {
            throw 'could not open file: ' + err;
        }

        // write the contents of the buffer, from position 0 to the end, to the file descriptor returned in opening our file
        fs.write(fd, buffer, 0, buffer.length, null, function(err) {
            if (err) throw 'error writing file: ' + err;
            fs.close(fd, function() {
                console.log('wrote the file successfully');
            });
        });
    });
};



const htmlClean = function() {
    return del([
        'dist/**/*.html'
    ]);
};


gulp.task('html:clean', htmlClean);
gulp.task('html:writeModules', writeInModules);
gulp.task('html', ['html:clean'] , htmlTask );


